services:
 - docker:1.13-dind

stages:
  - check
  - sanity
  - test
  - qa

variables:
  TEST_IMAGE: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-qa
  DOCKER_DRIVER: overlay
  DOCKER_HOST: tcp://docker:2375
  QA_SCREENSHOTS_DIR: $CI_PROJECT_DIR/screenshots
  QA_LOGS_DIR: $CI_PROJECT_DIR/logs

before_script:
  - bundle install
  - if [ -n "$TRIGGERED_USER" ] && [ -n "$TRIGGER_SOURCE" ]; then
      echo "Pipeline triggered by $TRIGGERED_USER at $TRIGGER_SOURCE";
    fi

check:rubocop:
  stage: check
  image: $TEST_IMAGE
  script: bundle exec rubocop
  except:
    - triggers
  tags:
    - docker

check:rspec:
  stage: check
  image: $TEST_IMAGE
  script: bundle exec rspec
  except:
    - triggers
  tags:
    - docker

.sanity: &sanity
  stage: sanity
  image: $TEST_IMAGE
  only:
    - schedules
  tags:
    - docker

ce:version:
  script: bin/qa Test::Sanity::Version CE
  allow_failure: true
  <<: *sanity

ee:version:
  script: bin/qa Test::Sanity::Version EE
  allow_failure: true
  <<: *sanity

.test: &test
  stage: test
  image: $TEST_IMAGE
  retry: 1
  tags:
    - docker
  artifacts:
    when: on_failure
    expire_in: 30d
    paths:
      - screenshots/
      - logs/

.high-capacity: &high-capacity
  tags:
    - docker
    - 7gb
    - triggered-packages

ce:instance:
    # Checking if the build is triggered for a EE image. In that case, we need
    # not run CE specific tests. This doesn't apply to nightly/regular branch
    # builds as RELEASE will not match gitlab-ce in those cases.
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ee); then exit 0; fi
    - bin/qa Test::Instance::Image ${RELEASE:=CE}
  <<: *test
  <<: *high-capacity

ee:instance:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ce); then exit 0; fi
    - bin/qa Test::Instance::Image ${RELEASE:=EE}
  <<: *test
  <<: *high-capacity

ce:image:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ee); then exit 0; fi
    - bin/qa Test::Omnibus::Image ${RELEASE:=CE}
  <<: *test

ee:image:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ce); then exit 0; fi
    - bin/qa Test::Omnibus::Image ${RELEASE:=EE}
  <<: *test

ce:update:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ee); then exit 0; fi
    - bin/qa Test::Omnibus::Update ${RELEASE:=CE}
  <<: *test
  <<: *high-capacity

ee:update:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ce); then exit 0; fi
    - bin/qa Test::Omnibus::Update ${RELEASE:=EE}
  <<: *test
  <<: *high-capacity

ce:upgrade:
  script:
    - if [[ $RELEASE ]]; then exit 0; fi
    - bin/qa Test::Omnibus::Upgrade
  <<: *test
  <<: *high-capacity

ce:mattermost:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ee); then exit 0; fi
    - bin/qa Test::Integration::Mattermost ${RELEASE:=CE}
  <<: *test
  <<: *high-capacity

ee:mattermost:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ce); then exit 0; fi
    - bin/qa Test::Integration::Mattermost ${RELEASE:=EE}
  <<: *test
  <<: *high-capacity

ee:geo:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ce); then exit 0; fi
    - bin/qa Test::Integration::Geo ${RELEASE:=EE}
  <<: *test
  <<: *high-capacity

ce:ldap:
  script:
    - bin/qa Test::Integration::LDAP ${RELEASE:=CE}
  <<: *test
  <<: *high-capacity

ee:ldap:
  script:
    - bin/qa Test::Integration::LDAP ${RELEASE:=EE}
  <<: *test
  <<: *high-capacity

qa:staging:
  script:
    - if $(echo ${RELEASE} | grep -q gitlab-ce); then exit 0; fi
    - export GITLAB_USERNAME="gitlab-qa"
    - export GITLAB_PASSWORD="$GITLAB_QA_PASSWORD"
    - unset EE_LICENSE
    - bin/qa Test::Instance::Staging
  <<: *test
  retry: 0
  stage: qa
  when: manual
  except:
    - triggers
